<?php

use App\Http\Controllers\VendaController;
use App\Http\Controllers\VendedorController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/venda', [VendaController::class, 'create']);
Route::get('/venda/{idvendedor}', [VendaController::class, 'list']);

Route::post('/vendedor', [VendedorController::class, 'create']);
Route::get('/vendedor', [VendedorController::class, 'list']);


