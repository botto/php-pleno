<?php

namespace Database\Factories;

use App\Models\Venda;
use App\Models\Vendedor;
use Illuminate\Database\Eloquent\Factories\Factory;

class VendaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Venda::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $value = $this->randomFloat(10, 30);

        return [
            'idvendedor' => Vendedor::factory(),
            'valor' => $value,
            'comissao_calculada' => ($value * 0.065),
            'comissao' => 0.065
        ];
    }

    private function randomFloat($min = 0, $max = 1) {
        return $min + mt_rand() / mt_getrandmax() * ($max - $min);
    }
}
