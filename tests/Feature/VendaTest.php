<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\Venda;
use Tests\TestCase;

class VendaTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_list_vendas()
    {
        $data_venda = Venda::factory()->make();
                
        $response = $this->get('/api/venda/1');
        $response->assertStatus(200);
    }

    public function test_criar_vendas_sucess()
    {
        $data_venda = Venda::factory()->make();
        $param = [ 'idvendedor' => $data_venda->idvendedor, 'valor' => $data_venda->valor, 'comissao_calculada' => $data_venda->comissao_calculada, 'comissao' => $data_venda->comissao];

        $response = $this->post('/api/venda', $param);
        $response->assertStatus(200);

        $this->assertDatabaseHas('vendas', [
            'idvendedor' => $data_venda->idvendedor, 
            'valor' => (string) $data_venda->valor,
            'comissao_calculada' => (string) number_format($data_venda->comissao_calculada, 4, '.', ''),
            'comissao' => (string) $data_venda->comissao
        ]);
    }

    public function test_criar_vendas_not_found_valor()
    {
        $data_venda = Venda::factory()->make();
        $param = [ 'idvendedor' => $data_venda->idvendedor];

        $response = $this->post('/api/venda', $param);
        $response->assertStatus(422);
        
        $response->assertJsonFragment(['0' =>  "Valor de venda é obrigatório"]);
       
    }
}
