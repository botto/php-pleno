<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\Vendedor;
use Tests\TestCase;

class VendedorTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_list_vendedor()
    {
        $response = $this->get('/api/vendedor');
        $response->assertStatus(200);
    }

    public function test_criar_vendedor_sucess()
    {
        $data_vendedor = Vendedor::factory()->make();
        $param = ['nome' => $data_vendedor->nome, 'email' => $data_vendedor->email];

        $response = $this->post('/api/vendedor', $param);
        $response->assertStatus(200);

        $this->assertDatabaseHas('vendedor', [
            'email' => $data_vendedor->email,
            'nome' => $data_vendedor->nome
        ]);
    }

    public function test_criar_vendedor_invalid_email()
    {
        $data_vendedor = Vendedor::factory()->make();
        $param = ['nome' => $data_vendedor->nome, 'email' => $data_vendedor->nome];

        $response = $this->post('/api/vendedor', $param);
        $response->assertStatus(422);
        
        $response->assertJsonFragment(['0' => "Formato de e-mail incorreto"]);

        $this->assertDatabaseMissing('vendedor', [
            'email' => $data_vendedor->email,
            'nome' => $data_vendedor->nome
        ]);
    }
}
