<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Vendedor;

class Venda extends Model
{    
    use HasFactory;

    protected $table = 'vendas';
    protected $fillable = [ 'valor', 'idvendedor', 'comissao_calculada', 'comissao' ];

    public function vendedor()
    {
        return $this->belongsTo(Vendedor::class);
    }
}
