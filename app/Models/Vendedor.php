<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Venda;

class Vendedor extends Model
{    
    use HasFactory;

    protected $table = 'vendedor';
    protected $fillable = [ 'nome', 'email' ];

    public function vendas()
    {
        return $this->hasMany(Venda::class);
    }
}
