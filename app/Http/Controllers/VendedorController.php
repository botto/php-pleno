<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;

use App\Http\Requests\CriarVendedorRequest;
use App\Models\Vendedor;

class VendedorController extends Controller
{
    function create(CriarVendedorRequest $request){

        try {
            $nome = $request->get('nome');
            $email = $request->get('email');
            $vendedor = Vendedor::create(['nome' => $nome, 'email' => $email]);

            Cache::forget('sellers');
            
            return response( $vendedor, 200);
            
        } catch (\Exception $th) {
            return response( $th->getMessage(), 400);    
        }
    }
    
    function list(){
        try {      
            
            $vendedores = Cache::get('sellers', function () {
                return Vendedor::all();                
            });

            return response( $vendedores, 200);    
            
        } catch (\Exception $th) {
            return response( $th->getMessage(), 400);    
        }
    }

}
