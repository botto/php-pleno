<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;

use App\Http\Requests\CriarVendaRequest;
use App\Models\Venda;

class VendaController extends Controller
{
    function create(CriarVendaRequest $request){
        try {
            $vendedor = $request->get('idvendedor');
            $valor = $request->get('valor');
            $porcentagem = 0.065;
            $comissao = ((float) $request->get('valor')) * 0.065;
            $venda = Venda::create([ 'idvendedor' => $vendedor, 'valor' => $valor, 'comissao_calculada' => $comissao, 'comissao' => $porcentagem]);

            Cache::forget('sells');
            
            return response( $venda, 200);
        } catch (\Exception $th) {
            return response( $th->getMessage(), 400);  
        }
    }
    
    function list($idvendedor){
        try {  
            if($idvendedor){
                $vendas = Cache::get('sells', Venda::where('idvendedor', $idvendedor)->get());
                return response( $vendas, 200);
            }
            else{
                return response('Informar código do vendedor', 400);     
            }
            
            return response( 'Parametro inválido', 400);  

        } catch (\Exception $th) {
            return response( $th->getMessage(), 400);  
        }
    }    
}
