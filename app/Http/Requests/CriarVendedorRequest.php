<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\ApiRequest;

class CriarVendedorRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required',
            'email' => 'required|email|unique:vendedor',
        ];
    }


    public function messages()
    {
        return [
            'nome.required' => 'Nome é obrigatório',
            'email.required' => 'E-mail é obrigatório',
            'email.email' => 'Formato de e-mail incorreto',
            'email.unique' => 'E-mail já cadastrado'
        ];
    }
}
