<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\ApiRequest;

class CriarVendaRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'idvendedor' => 'required',
            'valor' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'idvendedor.required' => 'ID do vendedor é obrigatório',
            'valor.required' => 'Valor de venda é obrigatório'
        ];
    }
}
